import { combineReducers } from '@reduxjs/toolkit';
import { counterReducer } from './counter/counter.reducer';
import { productsStore } from './products/products.store';
import { uiReducer } from './ui/ui.reducer';

export const orderReducers = combineReducers({
  ui: uiReducer,
  orderInfo: counterReducer,
  products: productsStore.reducer
})
