import { createReducer } from '@reduxjs/toolkit';
import { openLeftPanel, openRightPanel } from './ui.actions';

const initialState: { openLeftPanel: boolean, openRightPanel: boolean } = {
  openLeftPanel: false,
  openRightPanel: false
}
export const uiReducer = createReducer(initialState, b => b
  .addCase(openLeftPanel, (state) => {
    state.openRightPanel = false
    state.openLeftPanel = true
  })
  .addCase(openRightPanel, (state) => {
    return {
      openLeftPanel: false,
      openRightPanel: true
    }
  })

)
