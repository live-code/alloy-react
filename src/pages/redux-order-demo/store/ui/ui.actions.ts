import { createAction } from '@reduxjs/toolkit';

export const openLeftPanel = createAction('open left panel')
export const openRightPanel = createAction('open right panel')
