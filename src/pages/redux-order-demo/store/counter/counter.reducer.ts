import { createReducer } from '@reduxjs/toolkit';
import { logout } from '../../../../core/store/app.actions';
import * as CounterActions from './counter.actions';

interface CounterState {
  value: number,
  itemsPerPallet: number,
  material: 'wood' | 'plastic'
}

const initialState: CounterState = {
  value: 0,
  itemsPerPallet: 5,
  material: 'wood',
}
export const counterReducer = createReducer(initialState, b => b
    .addCase(CounterActions.inc, (state, action) => {
      return { ...state, value: state.value + action.payload }
    })
    .addCase(CounterActions.dec, (state, action) => {
      // immerjs
      state.value = state.value - action.payload;
      // return { ...state, value: state.value - action.payload }
    })
   .addCase(CounterActions.changeMaterial, (state, action) => {
     state.material = action.payload;
    })
    .addCase(CounterActions.changeItemPerPallet, (state, action) => {
      state.itemsPerPallet = action.payload;
    })

    .addCase(CounterActions.reset, () => ({ ...initialState }) )
    .addCase(logout, () => ({ ...initialState }) )
)
