import { createAction } from '@reduxjs/toolkit';

export const inc = createAction<number>('increment')
export const dec = createAction<number>('decrement')
export const reset = createAction('reset')
export const changeItemPerPallet = createAction<number>('change item per pallet')
export const changeMaterial = createAction<'wood' | 'plastic'>('change material')
