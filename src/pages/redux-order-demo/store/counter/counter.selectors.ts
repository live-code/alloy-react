import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../../../../index';

export const selectCounter = (state: RootState) => state.order.orderInfo.value;
export const selectMaterial = (state: RootState) => state.order.orderInfo.material;
export const selectItemPerPallet = (state: RootState) => state.order.orderInfo.itemsPerPallet;

export const selectDoubleCounter = (state: RootState) =>
  state.order.orderInfo.value * 2

export const selectTripleCounter = createSelector(
  selectCounter,
  (counter: number) => counter * 3
)

export const selectTotalPallet = createSelector(
  selectCounter,
  selectItemPerPallet,
  (count: number, itemsPerPallet: number) => Math.ceil(count / itemsPerPallet)
)

export const selectTotalPalletOLD = (state: RootState) =>
  Math.ceil(state.order.orderInfo.value / state.order.orderInfo.itemsPerPallet)
