import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../../../../model/product';

export const productsStore = createSlice({
  name: 'products',
  initialState: [] as Product[],
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      console.log(action.payload)
      return action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.push(action.payload)
    },
    // addProductFailed() {
    //   state.error = true;
    // },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.findIndex(p => p.id === action.payload)
      state.splice(index, 1)
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      const product = state.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
  }
});

export const {
  addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess
} = productsStore.actions;
