import { RootState } from '../../../../index';
import { Product } from '../../../../model/product';

export const selectProductsList = (state: RootState) => state.order.products;
export const selectProductsTotal = (state: RootState) => state.order.products
  .reduce((acc: number, cur: Product) => {
    return acc + cur.price;
  }, 0)
