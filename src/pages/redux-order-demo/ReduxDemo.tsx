import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../index';
import { Product } from '../../model/product';
import { inc } from './store/counter/counter.actions';
import * as CounterActions from './store/counter/counter.actions';
import { selectCounter, selectItemPerPallet, selectMaterial, selectTotalPallet } from './store/counter/counter.selectors';
import { selectProductsList } from './store/products/product.selectors';
import { addProduct, deleteProduct, getProducts } from './store/products/products.actions';
import { openLeftPanel, openRightPanel } from './store/ui/ui.actions';

export function ReduxDemo() {
  const dispatch = useDispatch() as AppDispatch;
  const counter = useSelector(selectCounter)
  const material = useSelector(selectMaterial)
  const itemsPerPallet = useSelector(selectItemPerPallet)
  const totalPallets = useSelector(selectTotalPallet);
  const isLeftPanelOpened = useSelector((state: RootState) => state.order.ui.openLeftPanel)
  const isRightPanelOpened = useSelector((state: RootState) => state.order.ui.openRightPanel)
  const products = useSelector(selectProductsList);

  useEffect(() => {
    dispatch(inc(10))
    dispatch(getProducts())
  }, [])

  function deleteHandler(e: React.MouseEvent<HTMLButtonElement>, id: number) {
    e.stopPropagation();
    dispatch(deleteProduct(id))
  }

  function addTodoHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addProduct({
        title: e.currentTarget.value,
        price: 2,
      }))
      e.currentTarget.value = '';
    }
  }

  return <div>


    <input type="text" onKeyDown={addTodoHandler} placeholder="add product name"/>
    {
      products.map((product: Product) => {
        return (
          <li
            className="list-group-item"
            key={product.id}
          >
            <span className="ml-2">{ product.title }</span>
            <button
              onClick={(e) => deleteHandler(e, product.id)}
            >delete</button>
          </li>
        )
      })
    }
    <hr/>

    {isRightPanelOpened && <div>RIGHT: {JSON.stringify(isRightPanelOpened)} <br/></div>}
    { isLeftPanelOpened && <div>LEft: {JSON.stringify(isLeftPanelOpened)} <br/></div>}
    <button onClick={() => dispatch(openRightPanel())}>open riight</button>
    <button onClick={() => dispatch(openLeftPanel())}>open left</button>
    <hr/>

    Products{counter} <br/>
    Total Pallets: {totalPallets} ({itemsPerPallet} per pallet of {material})
    <hr/>
    <button onClick={() => dispatch(CounterActions.dec(5))}>-</button>
    <button onClick={() => dispatch(CounterActions.inc(10))}>+</button>
    <button onClick={() => dispatch(CounterActions.reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(CounterActions.changeMaterial('plastic'))}>plastic</button>
    <button onClick={() => dispatch(CounterActions.changeMaterial('wood'))}>wood</button>
    <button onClick={() => dispatch(CounterActions.changeItemPerPallet(5))}>pallet 5 items</button>
    <button onClick={() => dispatch(CounterActions.changeItemPerPallet(10))}>pallet 10 items</button>

    <hr/>

  </div>
}
