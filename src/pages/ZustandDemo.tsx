import { useStore } from '../App';

export function ZustandDemo() {
  const counter = useStore((state) => state.counter)
  const inc = useStore((state) => state.increment)
  const changeLang = useStore((state) => state.changeLanguage)

  return <div>
    ZustandDemo {counter}
    <button onClick={inc}>+</button>
    <button onClick={() => changeLang('it')}>Change Lang IT</button>
    <button onClick={() => changeLang('en')}>Change Lang EN</button>
  </div>
}
