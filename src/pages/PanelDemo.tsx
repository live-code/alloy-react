import React, { useState } from 'react';
import { GoogleMap } from '../shared/GoogleMap';
import { MyButton } from '../shared/MyButton';
import { MyButtonGroup } from '../shared/MyButtonGroup';
import { Panel } from '../shared/Panel';

export function PanelDemo () {
  const [coords,setCoords] = useState({ lat: 43.397, lng: 13.644 })
  const [count, setCount] = useState(6)
  function gotoUrl() {
    console.log('gotoUrl')
  }

  console.log('Panel Demo render')
  return (
    <div className="m-4">
      <GoogleMap coords={coords} zoom={count} />
      <MyButtonGroup>
        <MyButton icon="💩" onClick={() => setCount(c => c+1)}>+</MyButton>
        <MyButton icon="😱" onClick={() => setCoords({lat: 44, lng: 13 })}>CHANGE MAP</MyButton>
      </MyButtonGroup>
      <Panel title="my profile" isFailed>
        <input type="text"/>
        <input type="text"/>
      </Panel>
      <Panel title="notification" isSuccess>
        <button>ciao</button>
      </Panel>

      <Panel title="notification" icon="fa fa-google" onIconClick={() => console.log('click')}>
        lorem
      </Panel>
      <Panel title="notification" icon="fa fa-facebook" onIconClick={gotoUrl} >
        lorem
      </Panel>

    </div>
  )
}
