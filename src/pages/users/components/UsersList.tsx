import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { User } from '../../../model/user';

interface UsersListProps {
  active: Partial<User> | null;
  items: Partial<User>[];
  onSelectUser: (user: Partial<User>) => void;
  onDeleteUser: (id: number) => void;
}

export const UsersList = ({ items, active, onDeleteUser, onSelectUser }: UsersListProps) => {
  return <>
    { items.length } users

    {
      items.map(u => {
        return (
          <li
            key={u.id}
            className={classNames('list-group-item', { active: u.id === active?.id})}
            onClick={() => onSelectUser(u)}
          >
            {u.name}
            <div className="pull-right">
              <Link to={`/users/${u.id}`}>
                <i className="fa fa-info-circle me-2 "></i>
              </Link>
              <i className="fa fa-trash"
                 onClick={(e) => {
                   e.stopPropagation();
                   onDeleteUser(u.id!)
                 }}></i>
            </div>
          </li>)
      })
    }
  </>
};
