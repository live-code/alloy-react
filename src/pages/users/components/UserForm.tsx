import { useEffect, useState, useTransition } from 'react';
import { User } from '../../../model/user';
import css from './UserForm.module.css';

interface UserFormProps {
  active: Partial<User> | null;
  onReset: () => void;
  onSave: (formData: Partial<User>) => void;
}
const initialState = { name: ''};

export const UserForm = (props:UserFormProps) => {
  const [formData, setFormData] = useState<Partial<User>>(initialState);

  useEffect(() => {
    if (props.active) {
      setFormData(props.active)
    } else {
      setFormData(initialState)
    }
  }, [props.active])

  function changeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setFormData({ ...formData, [e.currentTarget.name]: e.currentTarget.value })
  }
  function formHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSave(formData);
  }

  return <div className={css.myModal}>
    <form onSubmit={formHandler}>
      <input type="text" name="name" value={formData.name} onChange={changeHandler}/>
    </form>
    <button onClick={props.onReset}>reset</button>
  </div>
};
