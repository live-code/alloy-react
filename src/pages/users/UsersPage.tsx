import { UserForm } from './components/UserForm';
import { UsersList } from './components/UsersList';
import { useUsers } from './hooks/useUsers';

export default function UsersPage () {
  const { users, pending, activeUser, openModal, actions } = useUsers()

  return <div>
    { pending && <Pending /> }
    <hr/>
    <i
      className="fa fa-plus-circle fa-3x"
      onClick={actions.addNewUser}
    ></i>

    {openModal && <UserForm
      active={activeUser}
      onSave={actions.save}
      onReset={actions.resetUser}
    />}

    <UsersList
      items={users}
      active={activeUser}
      onDeleteUser={actions.deleteUser}
      onSelectUser={actions.selectUser}
    />

  </div>
};

export const Pending = () => <div>loading....</div>
