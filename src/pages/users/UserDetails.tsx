import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { log } from 'util';
import { User } from '../../model/user';

export function UserDetails() {
  const params = useParams<{id: string}>();
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    axios.get<User>(`http://localhost:3001/users/${params.id}`)
      .then(res => setUser(res.data))
  }, [])
  return <div>
    User details {params.id}

    <h1>{user?.name}</h1>
    <pre>{user?.phone}</pre>
  </div>
}
