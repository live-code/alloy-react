import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../../model/user';

export function useUsers() {
  const [users, setUsers] = useState<Partial<User>[]>([]);
  const [pending, setPending] = useState<boolean>(false)
  const [activeUser, setActiveUser] = useState<Partial<User> | null>(null)
  const [openModal, setOpenModal] = useState<boolean>(false)

  useEffect(() => {
    getUsers();
  }, [])

  function getUsers() {
    setPending(true)
    axios.get<User[]>('http://localhost:3001/users')
      .then(res => {
        setUsers(res.data);
        setPending(false);
      })
  }

  function save(formData: Partial<User>) {
    if(formData.id) {
      editUser(formData)
    } else {
      addUser(formData)
    }
  }
  function addUser(formData: Partial<User>) {
    axios.post<Partial<User>>(`http://localhost:3001/users/`, formData)
      .then(res => {
        // setUsers([...structuredClone(users), res.data])
        setUsers([...users, res.data])
      })
    closeModal();
  }

  function editUser(formData: Partial<User>) {
    axios.patch<Partial<User>>(`http://localhost:3001/users/${formData.id}`, formData)
      .then(res => {
        setUsers(
          users.map(u => {
            if (u.id === formData.id) {
              return res.data
            }
            return u;
          })
        );

        closeModal();
      })
  }

  function deleteUser(id: number) {
    axios.delete(`http://localhost:3001/users/${id}`)
      .then(() => {
        setUsers(
          users.filter(u => u.id !== id)
        );
        resetUser();
      })
  }
  function selectUser(u: Partial<User>) {
    setActiveUser(u);
    showModal();
  }
  function resetUser() {
    setActiveUser(null)
  }

  function showModal() {
    setOpenModal(true)
  }
  function closeModal() {
    setOpenModal(false)
  }
 function addNewUser() {
    resetUser();
    showModal()
  }

  return {
    users,
    pending,
    activeUser,
    openModal,
    actions: {
      selectUser,
      deleteUser,
      resetUser,
      save,
      addNewUser,
      closeModal,
      showModal
    }
  };
}
