import React, { useContext, useState } from 'react';

interface DashboardState {
  count: number;
  random: number;
}

export const DashboardContext = React.createContext<{ data: DashboardState, setData: any } | null>(null)

export  function ContextDemo() {
  console.log('------\nRoot: render')
  const [data, setData] = useState<DashboardState>({ count: 11, random: 22 })

  function inc() {
    setData(d => ({...d, count: d.count + 1 }))
  }

  return (
    <DashboardContext.Provider value={{ data, setData }} >
      <div className="comp">
        <h3>Demo Hooks: useReducer</h3>
        <Dashboard />
      </div>
    </DashboardContext.Provider>
  );
}


const Dashboard = () => {
  console.log(' Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel />
    <RandomPanel />
  </div>
}


const CounterPanel = React.memo(() => {
  console.log('   counter render')
  const state = useContext(DashboardContext)

  return <div className="comp">
    Count: {state?.data.count}
  </div>
})

const RandomPanel = React.memo(() => {
  console.log('   random render')
  const state = useContext(DashboardContext)
  return <div className="comp">
    Random Value: {state?.data.random}
    <button onClick={() =>  state?.setData((d: any) => ({...d, count: d.count + 1 }))}>+</button>
  </div>
})
