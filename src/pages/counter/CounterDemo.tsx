import React, { useCallback, useState } from 'react';

export function CounterDemo() {
  const [value, setValue] = useState<number>(10);
  const [users, setUsers] = useState<any[]>([])

  const inc = useCallback(() => {
    setValue(v => v + 1)
  }, []);

  return <div>
    <h1>{value}</h1>
    <button onClick={() => setUsers([1, 2, 3])}>set users {users.length}</button>
    <Counter value={value} onIncrement={inc}  />
  </div>
}

interface CounterProps {
  value: number;
  onIncrement: () => void;
}
const Counter = React.memo((props: CounterProps) => {
  console.log('render Counter component')
  return <div>
    counter is {props.value}
    <button onClick={props.onIncrement}>+</button>
  </div>
})


