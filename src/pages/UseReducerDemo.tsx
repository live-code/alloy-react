import React, { PropsWithChildren, useReducer } from 'react';


// useReducer + Composition

interface AppState {count: number;random: number; }
type ActionTypes = 'increment' | 'decrement' | 'random';

interface AppActions {
  type: ActionTypes,
  payload?: any
}

function appReducer(state: AppState, action: AppActions) {
  switch (action.type) {
    case 'increment': return { ...state, count: state.count + action.payload }
    case 'decrement': return { ...state, count: state.count - 1 }
    case 'random': return { ...state, random: Math.random() }
    default: throw new Error('unhandled action ' + action.type)
  }
}

export  function UseReducerDemo() {
  const [state, dispatch] = useReducer(appReducer, { count: 1, random: 2 })

  return (
    <div className="comp">
      <h3>Demo Hooks: useReducer {JSON.stringify(state)}</h3>
      <button onClick={() => dispatch({ type: 'decrement'})}>-</button>
      <button onClick={() => dispatch({ type: 'increment', payload: 20 })}>+</button>
      <button onClick={() => dispatch({ type: 'random'})}>random</button>
      <Dashboard>
        <CounterPanel>
          <SubCounterPanel value={state.count} />
        </CounterPanel>
        <RandomPanel value={state.random} />
      </Dashboard>
    </div>
  );
}

const Dashboard = (props: PropsWithChildren) => {
  console.log(' Dashboard: render')
  return <div className="comp">
    Dashboard
    {props.children}
  </div>
}

const CounterPanel = ((props: PropsWithChildren) => {
  console.log('  CounterPanel: render')
  return <div className="comp">
    {props.children}
  </div>
})

const SubCounterPanel = ((props: { value: number }) => {
  console.log('  SubCounterPanel: render')
  return <div className="comp">
    SubCount: {props.value}
  </div>
})

const RandomPanel = ((props: { value: number }) => {
  console.log('  Random Panel: render')
  return <div className="comp">
    Random Value: {props.value}

  </div>
})
