import { useLogin } from '../../../core/auth/auth.utils';

export const SignIn = () => {
  const {login} = useLogin()
  function loginHandler() {
    // ...
    login()
  }

  return <div>
    <h1>SignIn</h1>
    <input type="text"/>
    <input type="text"/>
    <button onClick={loginHandler}>LOGIN</button>
  </div>
};
