import { useEffect, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import { useChangePageEvent } from '../../core/auth/useChangePageEvent';

export const Login = () => {
  const [url, setUrl] = useState<string | null>(null);

  useEffect(() => {
    let lastUrl = window.location.href;
    const obs =  new MutationObserver(() => {
      const url = window.location.href;
      if (url !== lastUrl) {
        lastUrl = url;
        onUrlChange();
      }
    })

    obs.observe(document, {subtree: true, childList: true});

    return () => {
      setTimeout(() => {
        obs.disconnect();
      })
    }
  }, [url])

  function onUrlChange() {
    // console.log('onUrlChange', window.location.href)
    // prompt('occhio pagina cambiata')
  }

  return <div>
    <h1>Login</h1>

    <Outlet />



    <hr/>
    <Link to="registration">Registration</Link> |
    <Link to="lostpass">lostpass</Link> |
    <Link to="./">SignIn</Link>
  </div>
};



