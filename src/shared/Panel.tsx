import cn from 'classnames';
import React, { PropsWithChildren } from 'react';

interface PanelProps {
  title: string;
  icon?: string;
  url?: string;
  isSuccess?: boolean;
  isFailed?: boolean;
  onIconClick?: () => void;
}

export function Panel(props: PropsWithChildren<PanelProps>) {
  return <div className="card">
    <div
      className={cn('card-header', {'bg-danger': props.isFailed, 'bg-success': props.isSuccess})}
    >
      {props.title}

      {
        props.icon && (
          <div className="pull-right">
            <i className={props.icon} onClick={props.onIconClick}></i>
          </div>
        )
      }
    </div>
    <div className="card-body">{props.children}</div>
  </div>
}

