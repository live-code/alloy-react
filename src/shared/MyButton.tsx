import { PropsWithChildren } from 'react';

interface MyButtonProps {
  icon: string;
}
export function MyButton(props: PropsWithChildren<MyButtonProps & React.ButtonHTMLAttributes<HTMLButtonElement>>) {
  const { icon, children, ...rest } = props;
  return <button className="btn btn-primary" {...rest}>
    {icon}
    {children}
  </button>
}
