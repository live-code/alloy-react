import React, { useEffect, useRef } from 'react';

interface GoogleMapProps {
  coords: { lat: number, lng: number }
  zoom: number
}

let map: google.maps.Map;

export const GoogleMap = React.memo((props: GoogleMapProps) => {
  const mapEl = useRef<HTMLDivElement>(null);
  useEffect(() => {
    console.log('init map')
    map = new google.maps.Map(
      mapEl.current as HTMLDivElement,
    );

    return () => {
      // clean
    }
  }, [])


  useEffect(() => {
    console.log('update map')
    map.setCenter({
      lat: props.coords.lat,
      lng: props.coords.lng,
    })
  }, [props.coords.lat, props.coords.lng])


  useEffect(() => {
    console.log('update map')
    map.setZoom(props.zoom)
  }, [props.zoom])


  return <div>
    mappa
    <div ref={mapEl} style={{ width: 300, height: 200}}></div>
  </div>
})
