import { PropsWithChildren } from 'react';

export function MyButtonGroup(props: PropsWithChildren) {
  return <div className="btn-group">
    {props.children}
  </div>
}
