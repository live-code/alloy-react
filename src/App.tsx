import axios from 'axios';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter, Link, Navigate, Route, Routes } from 'react-router-dom';
import { Interceptor } from './core/auth/Interceptor';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { NavBar } from './core/NavBar';
import { logout } from './core/store/app.actions';
import { changeLanguage, changeTheme } from './core/store/config.store';
import { RootState } from './index';
import { ContextDemo } from './pages/ContextDemo';
import { LostPass } from './pages/login/components/LostPass';
import { Registration } from './pages/login/components/Registration';
import { SignIn } from './pages/login/components/SignIn';
import { Login } from './pages/login/Login';
import { ReduxDemo } from './pages/redux-order-demo/ReduxDemo';
import { UseReducerDemo } from './pages/UseReducerDemo';
import { UserDetails } from './pages/users/UserDetails';
import { CounterDemo } from './pages/counter/CounterDemo';
import { PanelDemo } from './pages/PanelDemo';
import { ZustandDemo } from './pages/ZustandDemo';
import create from 'zustand'

const UsersPage = React.lazy(() => import('./pages/users/UsersPage'))
const ContactsDemo = React.lazy(() => import('./pages/ContactsDemo'));

type Language = 'it' | 'en'

interface AppState {
  counter: number;
  language: Language;
  increment: () => void;
  changeLanguage: (val: Language ) => void;
}

export const useStore = create<AppState>((set) => ({
  counter: 0,
  language: 'it',
  increment: () => set((state) => ({ counter: state.counter + 1 })),
  changeLanguage: (language: Language) => {
    axios.patch('http://localhost:3001/config', { language })
      .then(() => {
        set(() => ({ language }))
      })
  }
}))


function App() {
  const [modal, setModal] = useState<boolean>(false);
  const config = useSelector((state: RootState) => state.config)
  const dispatch = useDispatch();

  console.log(config)
  return (
    <BrowserRouter>
      {
        modal &&  <div>Modal</div>
      }


      <Interceptor />
      <NavBar />
      <button onClick={() => dispatch(changeTheme('dark'))}>CHANGE TEMA</button>
      <button onClick={() => dispatch(changeLanguage('it'))}>CHANGE LANG</button>
      <button onClick={() => dispatch(logout())}>LOGOUT</button>


      <hr/>
      <Routes>
        <Route path="login" element={<Login />}>
          <Route index element={<SignIn />} />
          <Route path="lostpass" element={<LostPass />} />
          <Route path="registration" element={<Registration />} />
        </Route>
        <Route path="panels"  element={<PanelDemo />}  />
        <Route path="counter"  element={<CounterDemo />}  />
        <Route path="context"  element={<ContextDemo />}  />
        <Route path="zustand"  element={<ZustandDemo />}  />
        <Route path="redux"  element={<ReduxDemo />}  />
        <Route path="useReducer"  element={<UseReducerDemo />}  />

        <Route path="users"  element={
          <React.Suspense fallback={<div>loader....</div>}>
            <PrivateRoute
              redirectTo="/home"
              onDenyAccess={() => console.log('show modal')}>
              <UsersPage />
            </PrivateRoute>
          </React.Suspense>
        } />

        <Route path="users/:id"  element={<UserDetails />}  />
        <Route path="contacts"  element={
          <React.Suspense fallback={<div>...loading...</div>}>
            <ContactsDemo />
          </React.Suspense>
        }  />
        <Route path="home" element={ <div>Sono la home </div>} />
        <Route index element={<Navigate to="/home" />}/>
        <Route path="*" element={
          <>
            <div>pagina not exist</div>
            <Link to="/home">vai alla home</Link>
          </>
        } />
      </Routes>
    </BrowserRouter>
  )
}
export default App;

