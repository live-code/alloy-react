import { Action, AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css'
import { Provider } from 'react-redux';

import App from './App';
import { authStore } from './core/store/auth.store';
import { configStore } from './core/store/config.store';
import { orderReducers } from './pages/redux-order-demo/store';
import reportWebVitals from './reportWebVitals';

const rootReducer = combineReducers({
  config: configStore.reducer,
  auth: authStore.reducer,
  order: orderReducers,
  users: () => [],
})

export const store = configureStore({
  reducer: rootReducer
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>
export type AppDispatch = ThunkDispatch<RootState, any, AnyAction>;



const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

/*
subscribe zustand
const unsub1 = useStore.subscribe(res => {
  console.log('update', res)
})
*/
