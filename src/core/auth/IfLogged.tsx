import { PropsWithChildren } from 'react';
import { useLogin } from './auth.utils';

export const IfLogged = (props: PropsWithChildren) => {
  const { isLogged } = useLogin()

  return <>
    {
      isLogged() ?
        props.children : null
    }
  </>
};
