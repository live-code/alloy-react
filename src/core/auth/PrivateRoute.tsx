import { PropsWithChildren, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import { useLogin } from './auth.utils';

export interface PrivateRouteProps{
  onDenyAccess: () => void;
  redirectTo: string;
}

export const PrivateRoute = (props: PropsWithChildren<PrivateRouteProps>) => {
  const { isLogged } = useLogin()

  useEffect(() => {
    if (!isLogged) {
      props.onDenyAccess();
    }
  })

  return <div>
    {
      isLogged() ?
        props.children : <Navigate to={props.redirectTo} />
    }
  </div>
};
