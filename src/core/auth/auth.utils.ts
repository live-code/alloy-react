import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export function useLogin() {
  const navigate = useNavigate();

  function login() {
    axios.get('http://localhost:3001/login')
      .then(res => {
        localStorage.setItem('token', res.data.token)
        navigate('/home')
      })
  }

  function isLogged(): boolean {
    return !!localStorage.getItem('token')
  }


  function getToken(): string | null {
    return localStorage.getItem('token')
  }


  function logout() {
    localStorage.removeItem('token')
    navigate('/login')
  }

  return {
    login,
    logout,
    isLogged,
    getToken
  }
}




