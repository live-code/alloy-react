import axios, { AxiosRequestConfig } from 'axios';
import { useEffect, useState } from 'react';
import { useLogin } from './auth.utils';

export function useInterceptor() {
  const [errors, setErrors] = useState<any[]>([]);

  const { getToken } = useLogin();
  useEffect(() => {
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      // setError([...error]);
      // Do something before request is sent
      const tk = getToken();
      if (!!tk) {
        const cfg:  AxiosRequestConfig<any> = {...config}
        cfg.headers = {
          auth: 'Bearer '  + tk
        }
        return cfg;
      }
      return config;


    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });

// Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (e) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      setErrors(prev => [...prev, e]);

      return Promise.reject(e);
    });
  }, []);

  function removeError(index: number) {
    const newErrors = [...errors]
    newErrors.splice(index, 1)
    setErrors(newErrors)
  }

  return {
    errors,
    removeError
  }
}
