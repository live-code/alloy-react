import React from 'react';
import { useInterceptor } from './useInterceptor';

export  function Interceptor() {
  const { errors, removeError } = useInterceptor();
  return <div>
    {
      errors.map((e, index) => {
        return <div
          key={index} className="alert alert-danger">
          {e.response?.status === 404 && <div>server down</div>}
          {e.response?.status === 401 && <div>token expired</div>}
          <button onClick={() => removeError(index)}>Close</button>
        </div>
      })
    }
  </div>
}
