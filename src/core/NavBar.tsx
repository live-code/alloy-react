import classNames from 'classnames';
import { Link, NavLink } from 'react-router-dom';
import { useStore } from '../App';
import { useLogin } from './auth/auth.utils';
import { IfLogged } from './auth/IfLogged';

export const NavBar = () => {
  console.log('render navbar')
  const counter = useStore((state) => state.counter)
  const lang = useStore((state) => state.language)

  const { logout } = useLogin()
  const getActiveCls = ({ isActive} : { isActive: boolean }) => {
    return classNames('btn btn-primary', {'bg-dark': isActive})
  }

  return <div>
    {lang}
    <div className="btn-group">
      <NavLink to="/login" className={getActiveCls}>login</NavLink>
      <NavLink
        to="/panels" className="btn btn-primary"
        style={(obj) => {
          return obj.isActive ? { backgroundColor: 'orange' } : {}
        }}
      >
        panels
      </NavLink>
      <NavLink to="/counter" className={getActiveCls}>counter</NavLink>
      <IfLogged>
        <NavLink to="/users" className={getActiveCls}>users</NavLink>
      </IfLogged>
      <NavLink to="/contacts" className={getActiveCls}>contacts</NavLink>
      <NavLink to="/useReducer" className={getActiveCls}>useReducer</NavLink>
      <NavLink to="/context" className={getActiveCls}>context</NavLink>
      <NavLink to="/zustand" className={getActiveCls}>zustand</NavLink>
      <NavLink to="/redux" className={getActiveCls}>redux</NavLink>
      <div className="btn btn-primary" onClick={logout}>logout</div>
    </div>
  </div>
};
