import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { reset } from '../../pages/redux-order-demo/store/counter/counter.actions';
import { logout } from './app.actions';

const initialState = { language: 'en', theme: 'light'};

export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<'light' | 'dark'>) {
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<'it' | 'en'>) {
      state.language = action.payload;
    },
  },
  extraReducers: builder => builder
    .addCase(logout, (state, action) => {
      return { ...initialState}
    })
})

export const {
  changeTheme,
  changeLanguage,
} = configStore.actions;
