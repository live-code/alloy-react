import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { logout } from './app.actions';

const initialState = { token: 'weohfowehfoewh' };

export const authStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    signinSuccess(state, action: PayloadAction<{ token: string }>) {
      state.token = action.payload.token
    }
  },
  extraReducers: builder =>
    builder.addCase(logout, (state, action) => {
      state.token = ''
    })
})

export const {
  signinSuccess,
} = authStore.actions;

